class School:
    def __init__(self, name, address):
        self.name = name
        self.address = address
        self.classes = []

    def add_class(self, classroom):
        self.classes.append(classroom)

    def remove_class(self, classroom):
        self.classes.remove(classroom)

    def list_classes(self):
        for classroom in self.classes:
            print(f'Class: {classroom.number}')


class Classroom:
    def __init__(self, number):
        self.number = number
        self.students = []
        self.teachers = []

    def add_student(self, student):
        self.students.append(student)

    def remove_student(self, student):
        self.students.remove(student)

    def add_teacher(self, teacher):
        self.teachers.append(teacher)

    def remove_teacher(self, teacher):
        self.teachers.remove(teacher)

    def list_students(self):
        for student in self.students:
            print(f'Student: {student.first_name} {student.last_name}')

    def list_teachers(self):
        for teacher in self.teachers:
            print(f'Teacher: {teacher.first_name} {teacher.last_name}')


class Person:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name


class Student(Person):
    def __init__(self, first_name, last_name, age, student_number):
        super().__init__(first_name, last_name)
        self.age = age
        self.student_number = student_number

    def get_info(self):
        return f'{self.first_name} {self.last_name}, Age: {self.age}, Student Number: {self.student_number}'


class Teacher(Person):
    def __init__(self, first_name, last_name, subject):
        super().__init__(first_name, last_name)
        self.subjects = [subject]

    def add_subject(self, subject):
        self.subjects.append(subject)

    def remove_subject(self, subject):
        self.subjects.remove(subject)

    def get_info(self):
        return f'{self.first_name} {self.last_name}, Subjects: {", ".join(self.subjects)}'


class Subject:
    def __init__(self, name, weekly_hours):
        self.name = name
        self.weekly_hours = weekly_hours

    def get_info(self):
        return f'{self.name}, Weekly Hours: {self.weekly_hours}'


