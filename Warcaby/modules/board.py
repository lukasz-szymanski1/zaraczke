# board.py

class Board:
    def __init__(self):
        self.board = self.create_board()

    def create_board(self):
        # Tworzenie początkowej planszy
        board = [['.' for _ in range(8)] for _ in range(8)]
        for row in range(8):
            if row % 2 == 0:
                for col in range(0, 8, 2):
                    board[row][col] = '1' if row < 3 else 'A' if row > 4 else '.'
            else:
                for col in range(1, 8, 2):
                    board[row][col] = '1' if row < 3 else 'A' if row > 4 else '.'
        return board

    def display(self):
        print("   A B C D E F G H")
        for row in range(8):
            print(f"{row+1} ", " ".join(self.board[row]))

    def __getitem__(self, index):
        return self.board[index]

