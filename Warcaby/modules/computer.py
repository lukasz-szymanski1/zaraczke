# computer.py

import random

from modules.board import Board
from modules.capture import Capture


def find_moves(board, start_row, start_col):
    moves = []
    piece = board[start_row][start_col]

    if piece == '1':  # Zwykły pionek
        for end_row in range(8):
            for end_col in range(8):
                if abs(end_row - start_row) == 1 and abs(end_col - start_col) == 1:
                    if board[end_row][end_col] == '.':
                        moves.append(((start_row, start_col), (end_row, end_col)))

                if abs(end_row - start_row) == 2 and abs(end_col - start_col) == 2:
                    middle_row = (start_row + end_row) // 2
                    middle_col = (start_col + end_col) // 2
                    if board[middle_row][middle_col] == 'A':
                        capture = Capture(board, start_row, start_col, end_row, end_col, '1', 'A')
                        if capture.is_capture_possible(start_row, start_col, end_row, end_col, 'A'):
                            moves.append(((start_row, start_col), (end_row, end_col)))

    return moves


def make_move(board, move):
    start_row, start_col = move[0]
    end_row, end_col = move[1]
    piece = board[start_row][start_col]

    is_capture = abs(start_row - end_row) > 1 or abs(start_col - end_col) > 1

    if is_capture:
        if piece == '1':
            capture = Capture(board, start_row, start_col, end_row, end_col, piece, 'A')
            capture.perform_capture(start_row, start_col, end_row, end_col, piece, 'A')
            print(
                f"Komputer wykonał bicie: {chr(start_col + ord('a'))}{start_row + 1} -> {chr(end_col + ord('a'))}{end_row + 1}")
    else:
        board[start_row][start_col] = '.'
        board[end_row][end_col] = piece
        print(f"Komputer wykonał ruch: {chr(start_col + 97)}{start_row + 1} -> {chr(end_col + 97)}{end_row + 1}")


class ComputerPlayer:
    def __init__(self, piece='1'):
        self.piece = piece

    def computer_move(self, board: Board):
        possible_moves = []

        for start_row in range(8):
            for start_col in range(8):
                if board.board[start_row][start_col] == self.piece:  # Tylko pionki
                    moves = find_moves(board.board, start_row, start_col)
                    if moves:
                        possible_moves.extend(moves)

        if possible_moves:
            capture_moves = [
                move for move in possible_moves
                if abs(move[1][0] - move[0][0]) > 1 or abs(move[1][1] - move[0][1]) > 1
            ]

            if capture_moves:
                move = random.choice(capture_moves)
            else:
                move = random.choice(possible_moves)

            make_move(board.board, move)
        else:
            print("Komputer nie może wykonać ruchu")

