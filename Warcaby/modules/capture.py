# capture.py

class Capture:
    def __init__(self, board, start_row, start_col, end_row, end_col, player_piece, opponent_piece):
        self.board = board
        self.start_row = start_row
        self.start_col = start_col
        self.end_row = end_row
        self.end_col = end_col
        self.player_piece = player_piece
        self.opponent_piece = opponent_piece

    # Sprawdzanie, czy jest bicie
    def is_capture_possible(self, start_row, start_col, end_row, end_col, opponent_piece):
        # Check bounds
        if not (0 <= start_row < 8 and 0 <= start_col < 8 and 0 <= end_row < 8 and 0 <= end_col < 8):
            return False


        if abs(start_row - end_row) == 2 and abs(start_col - end_col) == 2:
            middle_row = (start_row + end_row) // 2
            middle_col = (start_col + end_col) // 2

            if self.board[middle_row][middle_col] == opponent_piece and self.board[end_row][end_col] == '.':
                return True

        return False

    # Wykonanie bicia
    def perform_capture(self, start_row, start_col, end_row, end_col, player_piece, opponent_piece):

        if not (0 <= start_row < 8 and 0 <= start_col < 8 and 0 <= end_row < 8 and 0 <= end_col < 8):
            print("Błąd: ruch poza granicami planszy")
            return False


        step_row = (end_row - start_row) // max(abs(end_row - start_row), 1)
        step_col = (end_col - start_col) // max(abs(end_col - start_col), 1)

        row, col = start_row + step_row, start_col + step_col
        opponent_found = False
        opponent_row, opponent_col = None, None


        while row != end_row or col != end_col:
            if self.board[row][col] == opponent_piece:
                if opponent_found:
                    print("Błąd: zbyt wiele pionków na trasie")
                    return False
                opponent_found = True
                opponent_row, opponent_col = row, col
            elif self.board[row][col] != '.':
                print("Błąd: pole na trasie zajęte")
                return False


            row += step_row
            col += step_col


        if not opponent_found:
            print("Błąd: brak pionka przeciwnika do zbicia")
            return False
        if self.board[end_row][end_col] != '.':
            print("Błąd: pole docelowe zajęte")
            return False


        self.board[opponent_row][opponent_col] = '.'
        self.board[end_row][end_col] = player_piece
        self.board[start_row][start_col] = '.'

        return True
