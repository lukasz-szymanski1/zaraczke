# player.py

from modules.capture import Capture
from modules.board import Board


class Player:
    def __init__(self, piece):
        self.piece = piece

    def move(self, board: 'Board', player_piece='A', opponent_piece='1'):

        while True:
            start = input("Podaj pole startowe (np. a3): ")
            end = input("Podaj pole docelowe (np. b4): ")

            if len(start) != 2 or len(end) != 2:
                print("Błąd! Zły format pola")
                continue

            if start[0] < 'a' or start[0] > 'h' or end[0] < 'a' or end[0] > 'h':
                print("Błąd! Kolumna powinna być literą od 'a' do 'h'.")
                continue

            if start[1] < '1' or start[1] > '8' or end[1] < '1' or end[1] > '8':
                print("Błąd! Wiersz powinien być liczbą od 1 do 8.")
                continue

            start_col = ord(start[0]) - ord('a')
            start_row = int(start[1]) - 1
            end_col = ord(end[0]) - ord('a')
            end_row = int(end[1]) - 1

            piece = board.board[start_row][start_col]
            if piece == 'A':
                pawn_capture = Capture(board.board, start_row, start_col, end_row, end_col, player_piece, opponent_piece)
                if pawn_capture.is_capture_possible(start_row, start_col, end_row, end_col, opponent_piece):
                    pawn_capture.perform_capture(start_row, start_col, end_row, end_col, piece, '1')
                    print(f"Bicie wykonane: {start} -> {end}")
                    break

                if abs(start_row - end_row) == 1 and abs(start_col - end_col) == 1:
                    if board.board[end_row][end_col] == '.':
                        board.board[start_row][start_col] = '.'
                        board.board[end_row][end_col] = 'A'
                        print(f"Ruch wykonany: {start} -> {end}")
                        break
                    else:
                        print("Błąd! Pole docelowe zajęte")
                else:
                    print("Błąd! Możesz poruszać się tylko na ukos")
            else:
                print("Błąd! Na tym polu nie masz swojego pionka")