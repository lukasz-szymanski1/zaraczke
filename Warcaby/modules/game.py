# game.py

from modules.board import Board
from modules.player import Player
from modules.computer import ComputerPlayer

class Game:
    def __init__(self):
        self.board = Board()
        self.winner = None
        self.player = Player('A')  
        self.computer = ComputerPlayer('1')  

    def check_winner(self):
        player_pieces = sum(row.count('A') for row in self.board.board)
        computer_pieces = sum(row.count('1') for row in self.board.board)

        if player_pieces == 0:
            self.winner = 'Komputer'
        elif computer_pieces == 0:
            self.winner = 'Gracz'
        return self.winner

    def start(self):
        while True:
            self.board.display()

            if not self.is_move_possible('A') and not self.is_move_possible('1'):
                print("Gra zakończona! Remis!")
                break

            print("Twój ruch:")
            self.player.move(self.board)

            if self.check_winner():
                print(f"Gra zakończona! Zwyciężył: {self.winner}")
                break

            print("Ruch komputera:")
            self.computer.computer_move(self.board)

            if self.check_winner():
                print(f"Gra zakończona! Zwyciężył: {self.winner}")
                break

    def is_move_possible(self, piece):
        for row in range(8):
            for col in range(8):
                if self.board.board[row][col] == piece:
                    directions = [(-1, -1), (-1, 1), (1, -1), (1, 1)]
                    for d_row, d_col in directions:
                        new_row, new_col = row + d_row, col + d_col
                        if 0 <= new_row < 8 and 0 <= new_col < 8 and self.board.board[new_row][new_col] == '.':
                            return True
        return False
