# checkers.py

from modules.game import Game

def start_game():
    game=Game()
    game.start()

if __name__ == "__main__":
    start_game()