import tkinter as tk

from modules.board import Board
from modules.computer import ComputerPlayer
from modules.player import Player

player = Player('A')
computer = ComputerPlayer('1')

# Stałe
EMPTY = 0
PLAYER = 1
COMPUTER = 2

root = tk.Tk()
root.geometry("800x600")
root.title("Warcaby")

canvas = tk.Canvas(root, width=800, height=600)
canvas.pack()

board = None

selected_piece = None


def initialize_board():
    global board
    board = Board()

    for row in range(8):
        for col in range(8):
            board.board[row][col] = '.'

    for row in range(3):
        for col in range(8):
            if (row + col) % 2 != 0:
                board.board[row][col] = '1'
    for row in range(5, 8):
        for col in range(8):
            if (row + col) % 2 != 0:
                board.board[row][col] = 'A'

    for row in board.board:
        print(' '.join(row))


def draw_board(board, canvas, size=8):
    if size <= 0:
        raise ValueError("Size must be greater than 0")

    canvas.delete("all")

    for row in range(size):
        for col in range(size):
            x1 = col * (800 / size)
            y1 = row * (600 / size)
            x2 = x1 + (800 / size)
            y2 = y1 + (600 / size)
            if (row + col) % 2 == 0:
                color = "white"
            else:
                color = "black"
            canvas.create_rectangle(x1, y1, x2, y2, fill=color)


            if row < len(board.board) and col < len(board.board[row]):
                piece = board.board[row][col]
                if piece == 'A':
                    draw_checker(x1 + (800 / (2 * size)), y1 + (600 / (2 * size)), "blue")
                elif piece == '1':
                    draw_checker(x1 + (800 / (2 * size)), y1 + (600 / (2 * size)), "red")




def draw_checker(x, y, color, size=60):
    x1 = x - size / 2
    y1 = y - size / 2
    x2 = x + size / 2
    y2 = y + size / 2
    canvas.create_oval(x1, y1, x2, y2, fill=color, outline="gray")


def on_click(event):
    global selected_piece

    col = int(event.x / (800 / 8))
    row = int(event.y / (600 / 8))

    print(f"Kliknięto pole: row={row}, col={col}")

    if selected_piece is None:
        if board.board[row][col] == 'A':
            selected_piece = (row, col)
            print(f"Wybrano pionek: {row}, {col}")
    else:
        prev_row, prev_col = selected_piece
        if board.board[row][col] == '.':
            start = (prev_row, prev_col)
            end = (row, col)

            valid = False
            try:
                player.move(board, player_piece='A', opponent_piece='1')
                valid = True
            except Exception as e:
                print(f"Błąd: {e}")

            if valid:
                print(f"Gracz wykonał ruch z {start} na {end}")

                draw_board(board, canvas)

                computer.computer_move(board)
                draw_board(board, canvas)

            selected_piece = None
        else:
            print(f"Pole jest zajęte")
            selected_piece = None


initialize_board()
draw_board(board, canvas)

canvas.bind("<Button-1>", on_click)

root.mainloop()
