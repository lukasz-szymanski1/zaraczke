# game.py
from modules.inventory import Inventory
from modules.home import Home, Wardrobe

# create instance of inventory nad home
player_inventory = Inventory()
home = Home()
wardrobe = Wardrobe()

# intro
def main():
        print('Znajdujesz się w ciemnym lesie. Co robisz ?')
        print('1. Pokaż stan zdrowia i zawartość plecaka')
        print('2. Maszeruj po ciemnku')
        choice = input('')

        if choice == '1':
                inventory()
        elif choice == '2':
                print('Wpadłeś do ciemniej dziury.....')

                player_inventory.take_damage(50)
                main()

# show inventory
def inventory():
        player_inventory.show_inventory()
        print('Co robisz?')
        print('1. Zapalam latarkę')
        print('2. Wyciągam scyzoryk')
        choice = input('')

        if choice == '1':
             turn_flashlight()
        elif choice == '2':
            use_knife()     

# use flashlight
def turn_flashlight():
        print('Rozświetliłeś sobie las. Rozlgądasz się...')
        print('Widzisz pustą chatkę. Postanawiasz do niej wejść.')
        home.show_home()
        home_action()


# use pocket knife
def use_knife():
        print('Wyjąłeś scyzoryk...')

        inventory()


# home action
def home_action():
        print('Co postanawiasz zrobić')
        print('1. Otwieram szafe')
        print('2. Ścielę łóżko')
        choice = input('')

        if choice == '1':
                wardrobe.show_contents()
                wardrobe_action()
                
        elif choice == '2':
                home.bed.make_bed()
                home_action()

                


# wardrobe action

def wardrobe_action():
       print('Co robisz ?')
       print('1. Pije wode')
       print('2. Wcinam jedzenie')
       choice = input('')
       if choice == '1':
        player_inventory.heal(30)
       elif choice == '2':
        player_inventory.heal(50)









main()