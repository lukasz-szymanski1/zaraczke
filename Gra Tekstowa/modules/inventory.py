#  inventory.py


# class Inventory
class Inventory:

# main information
    def __init__(self):
        self.health = 100
        self.items = ['latarka', 'scyzoryk']

# show inventory content 
    def show_inventory(self):
        print(f'Stan zdrowia: {self.health}')
        print('Przemioty w plecaku:')
        for item in self.items:
            print(f'- {item}')

# add item to inventory
    def add_item(self, item):
        self.items.append(item)
        print(f'Dodano {item} do plecaka.')


# remove item from inventory
    def remove_item(self, item):
        if item in self.items:
            self.items.remove(item)
            print(f'Usunięto {item} z plecaka.')
        else:
            print(f'{item} nie znajduje się w plecaku.')

# take damage 
    def take_damage(self, amount):
        self.health -= amount
        if self.health < 0:
            self.health = 0
        print(f'Otrzymałeś {amount} obrażeń. Aktualny stan zdrowia: {self.health}')

# heal your health
    def heal(self, amount):
        self.health += amount
        if self.health > 100:
            self.health = 100
        print(f'Odzyskałeś {amount} zdrowia. Aktualny stan zdrowia: {self.health}')

        