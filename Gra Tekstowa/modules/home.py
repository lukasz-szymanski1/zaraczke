# home.py


# class Home
class Home: 
    def __init__(self):
        self.wardrobe = Wardrobe()
        self.bed = Bed()

# show home content
    def show_home(self):
        print('W domu znajduje się')
        print("Szafa oraz Łóżko")



# class Wardrobe
class Wardrobe:
    def __init__(self):
        self.items = ['butelka wody', 'puszka z jedzeniem']


# show wardrobe content
    def show_contents(self):
        print("Przedmioty w szafie:")
        for item in self.items:
            print(f'- {item}')



# class Bed
class Bed:
    def __init__(self):
        self.made = False
    
# make bed def
    def make_bed(self):
        self.mage = True
        print('Łóżko zostało pościelone')

# show bed state
    def show_state(self):
        if self.made:
            print('Łóżko zostało pościelone')
        else:
            print('Łóżko nie zostało pościelone')
