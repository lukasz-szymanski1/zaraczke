import random 

def choose_word():
    words = ['python', 'computer', 'code', 'framework', 'function', 'syntax', 'debugging', 'programming', 'syntax', 'module'  ]
    return random.choice(words)

def display_word(word, guessed_letters):
    display = ''
    for letter in word:
        if letter in guessed_letters:
            display += letter + ' '
        else:
            display += '_ '
    return display.strip()

def display_hangman(tries):
    stages = [
        """
                   --------
                   |      |
                   |      O
                   |     \\|/
                   |      |
                   |     / \\
                   -
                """,
        """
                   --------
                   |      |
                   |      O
                   |     \\|/
                   |      |
                   |     / 
                   -
                """,
        """
                   --------
                   |      |
                   |      O
                   |     \\|/
                   |      |
                   |      
                   -
                """,
        """
                   --------
                   |      |
                   |      O
                   |     \\|
                   |      |
                   |     
                   -
                """,
        """
                   --------
                   |      |
                   |      O
                   |      |
                   |      |
                   |     
                   -
                """,
        """
                   --------
                   |      |
                   |      O
                   |    
                   |      
                   |     
                   -
                """,
        """
                   --------
                   |      |
                   |      
                   |    
                   |      
                   |     
                   -
                """
    ]
    return stages[tries]


def play_again():
    while True:
        again = input("Do you want to play again? (y/n): ").lower()
        if again == "y":
            return True
        elif again == "n":
            return False
        else: 
            print("Invalid input. Please enter 'y or 'n'.")

def hangman():
    while True:
        word = choose_word()
        guessed_letters = []
        tries = 6

        print("Welcome to Hangman!")
        print("Try yo guess the words")

        while tries > 0:
            print("\n" + display_word(word, guessed_letters))
            print(display_hangman(tries))
            guess = input("Enter a letter: ").lower()

            if guess in guessed_letters:
                print("You've already guessed that letter.")
            elif guess in word:
                guessed_letters.append(guess)
                if set(word) == set(guessed_letters):
                    print("\nCongatulations! You guessed the word:", word)
                    break
            else:
                print("Incorrect guess.")
                tries -= 1
                print("Tries left:", tries)
    
        if tries == 0:
            print("\nYou lose! The word was:", word)
        
        if not play_again():
            break

hangman()
